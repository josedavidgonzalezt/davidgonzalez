import React,{Fragment,useState} from 'react';
import cerrar from '../img/close.svg';

const Imagen = ({portada}) => {

    const {enlace,imagen}= portada;
      //state que activará las clases show y showImage
      const [mostrar, actMostrar]= useState(false);

      //acción del onclick
      const elegirImagen=()=>{
          actMostrar(true);
      }
       
      //Función para validar
      let show;
      let showImage;
      const mostrarClase =()=>{
          if(mostrar===true){
              show= 'show';
              showImage='showImage';
              return show && showImage;
          }
      }
      mostrarClase();
  
      //Funcion para que al hacer click, ocultar la imagen
      const ocultarImagen=()=>{
          actMostrar(false); 
      }
    return ( 
        <Fragment>
            <img 
                className="img-galeria" 
                src={imagen} alt="imagen de la galeria" 
                onClick={()=>elegirImagen()}
            />
            
            <div className={`imagen-ligth ${show}`}> 
                <img src={cerrar} alt="icono para cerrar la imagen" className="close" onClick={()=>ocultarImagen()}/>
                <img src={imagen} alt="imagen en grande" className={`agregar-img ${showImage}`}/>
                <div class="visitar" >
                    <a href={enlace} rel="noopener noreferrer" target="_blank">
                        <p>Ver Aqui</p>
                    </a>
                </div>
            </div>
        </Fragment>
     );
}
 
export default Imagen;