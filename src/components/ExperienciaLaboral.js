import React from 'react';
import mrmedia from '../img/mr_media.jpg';
import arcoservices from '../img/arco_services.jpg';
import gcba from '../img/gcba.jpg';
import wilab from '../img/wilab.jpg';

const ExperienciaLaboral = () => {
    return ( 
        <section className="contenedor" id="experiencia">
                <h2 className="subtitulo">Experiencia Laboral En:</h2>
                <section className="experts">
                    <div className="cont-expert">
                        <img className="img-experts" src={mrmedia} alt="logo mr media"/>
                        <h3 className="n-expert">Mr Media</h3>
                    </div>
                    <div className="cont-expert">
                        <img className="img-experts" src={arcoservices} alt="loco arco services"/>
                        <h3 className="n-expert">Arco Services</h3>
                    </div>
                    <div className="cont-expert">
                      <img className="img-experts" src={gcba} alt="logo gcba"/>
                        <h3 className="n-expert">GCBA </h3>
                    </div>
                    <div className="cont-expert">
                      <img className="img-experts" src={wilab} alt="logo gcba"/>
                        <h3 className="n-expert">Wilab </h3>
                    </div>
                </section>
        </section>
     );
}
 
export default ExperienciaLaboral;