import React from 'react';

const Servicios = () => {

    const technologies = ['HTML', 'CSS', 'JavaScript', 'Reactjs', 'Firebase', 'Graphql', 'Nodejs', ]
    
    return ( 
        <section className="services contenedor" id="tecnologias">
                <h2 className="subtitulo">Tecnologías</h2>
                <div className="contenedor-servicio">
                    <img src='https://quirky-curran-b6bd1f.netlify.app/img/checklist_isometric.svg' alt="imagen isometrica"/>
                    <div className="checklist-servicio">
                        {technologies.map((technology, i) => (
                            <div className="service">
                                <h3 className="n-service"><span class="number">{i+1}</span>{technology}</h3>
                                {/* <p>Maquetado de paginas web</p> */}
                            </div>
                        ))}
                    </div>
                </div>
            </section>
     );
}
 
export default Servicios;