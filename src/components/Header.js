import React from 'react';
import hamburguesa from '../img/hamburguesa.svg';

const Header = ({spread,actSpread}) => {

    const primerClick =()=>{
        actSpread(true)
    }

    const segundoClick =()=>{
        actSpread(false)
    } 

    let mostrar;
    const verMenu=()=>{
        if (spread===true){
            mostrar='spread'
            return mostrar
        }
        else{return null}
    }
    verMenu();

    return ( 
        <header className="header" id="inicio">
             <img src={hamburguesa} alt="" className="hamburguer" onClick={()=>primerClick()} />
              <nav className={`menu-navegacion ${mostrar}`} onClick={()=>segundoClick()}>
                    <a href="#inicio">Inicio</a>
                    <a href="#tecnologias">Tecnologías</a>
                    <a href="#portafolio">Portafolios</a>
                    <a href="#experiencia">Experiencia</a>
                    <a href="#contacto">Contacto</a>
                </nav>
            <div className="contenedor head" onClick={()=>segundoClick()}>
                <h1 className="titulo">TODO ES POSIBLE PARA EL QUE CREE</h1>
                <p className="copy">¡Nunca te detengas por los que no creen en ti!</p>
            </div>
        </header>

     );
}

 
export default Header;