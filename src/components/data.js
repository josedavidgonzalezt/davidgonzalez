import mapa from '../img/mapa_gcba.jpg';
import weather from '../img/clima.jpg';
import tasaday from '../img/tasaday.jpg';
import budgets from '../img/budgets.jpg';
import insurance from '../img/insurance.jpg';
import covid from '../img/covid-19.jpg';
import crypto from '../img/criptomonedas.jpg';
import voluntarios from '../img/voluntarios.jpg';
import countries from '../img/countries.jpg';
import pixabay from '../img/pixabay.jpg';
import canciones from '../img/canciones.jpg';
import breaking from '../img/breakingbad.jpg';
import despertarSpa from '../img/despertarspa.gif';
import recipedrink from '../img/recipedrink.gif';
import merntask from '../img/merntask.gif';
import lamatanza from '../img/lamatanza.gif';
 const datos=[

        {
            "nombre":"Obras La Matanza",
            "enlace":"#",
            "imagen": `${lamatanza}`
        },
        {
            "nombre":"MERN TASK",
            "enlace":"https://mern-task-build.netlify.app/",
            "imagen": `${merntask}`
        },
        {
            "nombre":"Despertar Spa",
            "enlace":"https://despertarspa.com/",
            "imagen": `${despertarSpa}`
        },
        {
            "nombre":"Recipe Drink",
            "enlace":"https://recipedrinks.netlify.app/",
            "imagen": `${recipedrink}`
        },
        {
            "nombre":"Mapa-edificios",
            "enlace":"https://sitiosbada.netlify.app/",
            "imagen": `${mapa}`
        },
        {
            "nombre":"This weather",
            "enlace":"https://thisweather.netlify.app/",
            "imagen": `${weather}`
        },
        {
            "nombre":"Tasaday",
            "enlace":"https://tasaday.netlify.app/",
            "imagen": `${tasaday}`
        },
        {
            "nombre":"This insurance quote",
            "enlace":"https://thisinsurancequote.netlify.app/",
            "imagen": `${insurance}`
        },
        {
            "nombre":"My budgets",
            "enlace":"https://mybudgets.netlify.app/",
            "imagen":`${budgets}`
        },
        {
            "nombre":"Sars-cov2",
            "enlace":"https://sars-cov2.netlify.app/",
            "imagen": `${covid}`
        },
        {
            "nombre":"My crypto quote",
            "enlace":"https://mycryptoquote.netlify.app/",
            "imagen": `${crypto}`
        },
        {
            "nombre":"Quotes breaking bad",
            "enlace":"https://quotesbreakingbad.netlify.app/",
            "imagen": `${breaking}`
        },
        {
            "nombre":"Registro voluntarios",
            "enlace":"https://registrovoluntarios.netlify.app/",
            "imagen": `${voluntarios}`
        },
        {
            "nombre":"Data countries",
            "enlace":"https://datacountries.netlify.app/",
            "imagen": `${countries}`
        },
        {
            "nombre":"Pixaimages",
            "enlace":"https://pixaimages.netlify.app/",
            "imagen": `${pixabay}`
        },
        {
            "nombre":"Letra de canciones",
            "enlace":"https://letradecanciones.netlify.app/",
            "imagen": `${canciones}`
        }
    ];

    export default datos;