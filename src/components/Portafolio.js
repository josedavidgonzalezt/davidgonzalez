import React,{Fragment} from 'react';
import datos from './data';
import Imagen from './Imagen';

const Portafolio = () => { 

    return ( 
        <Fragment>
        <section className="gallery" id="portafolio">
                <div className="contenedor">
                    <h2 className="subtitulo">Portafolio</h2>
                    <div className="contenedor-galeria">
                        {datos.map(portada=>(
                            <Imagen
                                key={portada.enlace}
                                portada={portada}
                            />
                        ))}
                    </div>
                </div>
            </section>
        </Fragment>
     );
}
 
export default Portafolio;