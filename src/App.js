import React,{Fragment,useState} from 'react';
import Header from './components/Header';
import Servicios from './components/Servicios';
import Portafolio from './components/Portafolio';
import ExperienciaLaboral from './components/ExperienciaLaboral';


/* 61746921 */
function App() {
   
  const [spread, actSpread] = useState(false);

  const segundoClick =()=>{
    actSpread(false)
  } 

  return (
    <Fragment>
      <Header
        spread={spread}
        actSpread={actSpread}
      />

      <div 
        onClick={()=>segundoClick()}
      >
        <main>
            <Servicios/>
            <Portafolio/>
            <ExperienciaLaboral/>
        </main>
        <footer id="contacto">
            <div className="contenedor footer-content">
                <div className="contac-us">
                    <h2 className="brand">David González</h2>
                    <p>Nada es imposible</p>
                </div>
                <div class="social-media">
                    <a href="https://twitter.com/davidgnzalez" class="social-media-icon">
                        <i className='bx bxl-twitter'></i>
                    </a>
                    <a href="https://www.instagram.com/davidgnzalez/" class="social-media-icon">
                        <i className='bx bxl-instagram'></i>
                    </a>
                    <a href="https://www.linkedin.com/in/josedavidgonzaleztineo/" class="social-media-icon">
                      <i class='bx bxl-linkedin'></i>
                    </a>
                </div>
      
            <div className="line">

            </div>
            </div>
        </footer>
      </div>
       
    </Fragment>
  );
}

export default App;
